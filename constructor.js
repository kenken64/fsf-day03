/**
 * Created by phangty on 12/10/16.
 */


var newObject = {};
console.log(newObject);
var newObject2 = { name: "kenneth"};
console.log(newObject2);

var newObject3 = Object.create(Object.prototype);
newObject3.name = "Alex";
newObject3.age = 18;

console.log(newObject3);

var newObject4 = new Object();
//newObject4.name = "Sandy";
console.log(newObject4);

console.log(newObject4.name);
console.log(newObject4['name']);

// manipulate the attribute of the newly created object "name"
Object.defineProperty(newObject4, "name",{
   value: "Phang",
    writable: true,
    enumerable: false,
    configurable: true
});

//newObject4.name = 'Kenneth';
console.log(newObject4['name']);
console.log(newObject4.name);
console.log(newObject4.name);

var o = {};
Object.defineProperty(o, "a", {value: '1', enumerable: true});
Object.defineProperty(o, "b", {value: '2', enumerable: false});
Object.defineProperty(o, "c", {value: '3',enumerable: true, configurable: false});
console.log(o);
console.log(o.b);
delete o.a;
delete o.c;
console.log(o);

for(var i in o){
    console.log(i);
}





