/**
 * Created by phangty on 12/10/16.
 */
/**
 * Created by phangty on 12/10/16.
 */
var myPromise = new Promise(function(resolve, reject){
    setTimeout(function(){
        resolve("12 13 34 13 45");
    }, 2000);
});

function thenResult(){
    console.log("Accepted");
}

function thenRejected(){
    console.log("Rejected");
}

myPromise
    .then(thenResult)
    .catch(thenRejected);

//SEPARATE EXAMPLE
function isValid(colour){
    return new Promise(function(resolve2, reject){
        if(colour=='BLUE'){
            //RESOLVE("SOME VALUE")
            resolve2("Valid colour");
        } else if(colour == 'RED'){
            resolve2('valid colour');
        } else{
            reject("invalid colour");
        }
    });
}

isValid("YELLOW")
    .then(function(goodnews)
    {
        console.log(goodnews);
    }).catch(function(badnews){
    console.log(badnews);
});

var obj2 = {
    name: "",
    age: 0,
    say: function (prefix){
        console.log("Say !");
        console.log(prefix);
        console.log(this.name);
        return prefix + ' ' + this.name + ' ' + this.age;
    }
};

var anotherObj = {name: "Jack", age: 22};
var returnValue = obj2.say.call(anotherObj, "Hello");
console.log(returnValue);