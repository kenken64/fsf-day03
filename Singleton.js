/**
 * Created by phangty on 12/10/16.
 */
// singleton.js
var singleton= require('./a-singleton');

console.log(singleton.someProperty);
console.log(singleton.toggleZ(1));
console.log(singleton.toggleZ(2));
