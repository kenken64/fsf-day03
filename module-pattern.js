/**
 * Created by phangty on 12/10/16.
 */
// module-pattern.js
var a = require('./a.js');
var b = require('./b.js');
var c = require('./x/c.js');

//console.log(a);
//console.log(a.bWasloaded);
//console.log(a.age);
console.log(a.loaded2);
//console.log(a.loaded3);

//console.log(a.age);

console.log(a.drive());
//console.log();

var x = b.run(function(result){
    console.log(result);
});
console.log(x);

console.log(c);
//console.log(a.bWasloaded);

/*
console.log(b);
console.log(b.aWasloaded);
*/
