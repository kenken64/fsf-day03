/**
 * Created by phangty on 12/10/16.
 */
var Subject = function() {
    // List of observers that is subscribed to
    var observers = [];

    return {
        subscribeObserver: function(observer) {
            observers.push(observer);
        },
        unsubscribeObserver: function(observer) {
            var index = observers.indexOf(observer);
            if(index > -1) {
                observers.splice(index, 1);
            }
        },
        notifyObserver: function(observer) {
            var index = observers.indexOf(observer);
            if(index > -1) {
                observers[index].notify(index);
            }
        },
        notifyAllObservers: function() {
            for(var i = 0; i < observers.length; i++){
                observers[i].notify(i);
            };
        }
    };
};

var Observer = function() {
    return {
        notify: function(index) {
            console.log("Observer " + index + " is notified!");
        }
    }
}

var subject = new Subject();

var observer0 = new Observer();
var observer1 = new Observer();
var observer2 = new Observer();
var observer3 = new Observer();

// Subscribe to several observers
subject.subscribeObserver(observer0);
subject.subscribeObserver(observer1);
subject.subscribeObserver(observer2);
subject.subscribeObserver(observer3);

var x = 8
if(x == 8){
    console.log("Send out info using everything !");
    subject.notifyAllObservers();
}

var y = 9;
if(y == 9){
    subject.notifyObserver(observer2);
}
