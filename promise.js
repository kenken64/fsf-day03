/**
 * Created by phangty on 12/10/16.
 */
/*
var myPromise = new Promise(function(resolve, reject){
  setTimeout(function(){
      resolve("12 13 34 13 45");
  }, 2000);
});

function thenResult(){
    console.log("Accepted");
}

function thenRejected(){
    console.log("Rejected");
}

myPromise
    .then(thenResult)
    .catch(thenRejected);

//SEPARATE EXAMPLE
function isValid(colour){
    return new Promise(function(resolve2, reject){
       if(colour=='BLUE'){
           //RESOLVE("SOME VALUE")
           resolve2("Valid colour");
       } else if(colour == 'RED'){
           resolve2('valid colour');
       } else{
           reject("invalid colour");
       }
    });
}

isValid("YELLOW")
    .then(function(goodnews)
    {
        console.log(goodnews);
}).catch(function(badnews){
    console.log(badnews);
});*/
/*

var obj = {
    name: "",
    age: 0,
    email: "",
    say : function(prefix, value2, value3){
        console.log(prefix);
        console.log(this.name);
        console.log(prefix + ' ' + this.name);
        console.log(value2 + ' ' + this.name);
        console.log(value3 + ' ' + this.name);
        return prefix + ' ' + value2 + ' ' + this.name + ' ' + this.age;
    }
};

var newObject = { name: "Alex", age: 33};
var resultx = obj.say.call(newObject, "Hello",
    "How are you", "test1", "test3", "test5");
console.log(resultx);

var newObject2 = { name: "Sam", age: 45};
var resultx2 = obj.say.call(newObject2, "Hello",
    "How are you", "test1", "test3", "test5");
console.log(resultx2);

*/

var obj2 = {
    name: "",
    age: 0,
    email: "",
    siblings: [],
    say : function(greeting1, greeting2){
        console.log(greeting1);
        console.log(greeting2);
        console.log(this.siblings);

        console.log(this.name);
        return greeting1 + ' ' + this.name + ' ' + this.age;
    }
};

var newObject2 = { name: "Sam", age: 45, siblings: ['Mary', 'Jane']};
var result4= obj2.say.apply(newObject2, ['Hello', 'there']);
console.log(result4)