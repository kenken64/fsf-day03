/**
 * Created by phangty on 12/10/16.
 */
// prototype1.js
var TelsaModelS = function() {
    this.numWheels = 4;
    seats = 5;
    this.manufacturer = "telsa";
    this.make = "ModelS";
    var belts = 5;
}

TelsaModelS.prototype = function(){
    var go = function(){
        console.log("rotate wheels");
    };

    var stop = function(){
        console.log("stop wheels");
    };

    return {
        pressBrakePedal: stop,
        jamBrake: stop,
        pressGasPedal: go
    }
}();

module.exports = TelsaModelS;