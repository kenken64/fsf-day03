/**
 * Created by phangty on 12/10/16.
 */
var profiler = require('./profiler');

function getRandomArray(len) {
    // Instantiate the profiler
    var p = profiler('Generating a ' + len + ' items long array');

    // Start the profiler
    p.start();
    var arr = [];     // empty array
    // Populate the empty array with random numbers
    for(var i = 0; i < len; i++) {
        arr.push(Math.random());
    }
    // End the profiler
    p.end();
}

// Use the function
getRandomArray(1e6);
console.log('Done');

